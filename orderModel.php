<?php
require_once("dbconfig.php");

function getOrderList($uID) {
	global $db;
	$sql = "SELECT ordID, orderDate, status FROM userOrder WHERE uID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function getConfirmedOrderList() {
	global $db;
	$sql = "SELECT ordID, uID, orderDate FROM userOrder WHERE status=1";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	//mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function _getCartID($uID) {
	//get an unfished order (status=0) from userOrder
	global $db;
	$sql = "SELECT ordID FROM userorder WHERE uID=? and status=0";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	if ($row=mysqli_fetch_assoc($result)) {
		return $row["ordID"];
	} else {
		//no order with status=0 is found, which means we need to creat an empty order as the new shopping cart
		$sql = "insert into userOrder ( uID, status ) values (?,0)";
		$stmt = mysqli_prepare($db, $sql); //prepare sql statement
		mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
		mysqli_stmt_execute($stmt);  //執行SQL
		$newOrderID=mysqli_insert_id($db);
		return $newOrderID;
	}
}

function addToCart($uID, $prdID) {
	global $db;
	$ordID= _getCartID($uID);
	$usql = "SELECT * FROM orderItem WHERE prdID = '".$prdID."' AND ordID ='".$ordID."';";
	$stmt = mysqli_query($db, $usql);
	if(mysqli_num_rows($stmt) == 1) {
		$sql = "UPDATE orderItem SET quantity = quantity + 1 WHERE prdID = '".$prdID."' AND ordID ='".$ordID."';";
		$stmt = mysqli_prepare($db, $sql);
		mysqli_stmt_execute($stmt);
	} else {
	$sql = "insert into orderItem (ordID, prdID, quantity) values (?,?,1);";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "ii", $ordID, $prdID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
	}
}

function removeFromCart($uID, $prdID) {
	global $db;
	$ordID= _getCartID($uID);
	// 找出有一行pid和ordid一樣的
	$asql = "SELECT * FROM orderItem WHERE prdID = '".$prdID."' AND ordID ='".$ordID."';";
	// 找出有一行pid和ordid一樣的，不過數量=1
	$bsql = "SELECT quantity FROM orderItem WHERE prdID = '".$prdID."' AND ordID ='".$ordID."' AND quantity = '1';";
	$stmt = mysqli_query($db, $asql);
	$stmu = mysqli_query($db, $bsql);
	if (mysqli_num_rows($stmu) == 1) {
		$csql = "DELETE FROM orderItem WHERE prdID = '".$prdID."' AND ordID ='".$ordID."';";
		$stmt = mysqli_prepare($db, $csql);
		mysqli_stmt_execute($stmt);
		echo "<td>item removed</td>";
	}
	else if(mysqli_num_rows($stmt) == 1) {
		$sql = "UPDATE orderItem SET quantity = quantity - 1 WHERE prdID = '".$prdID."' AND ordID ='".$ordID."';";
		$stmt = mysqli_prepare($db, $sql);
		mysqli_stmt_execute($stmt);
		echo "<td>item removed</td>";
	}
	else {
		echo "<td>cart empty</td>";
	} 
}

function checkout($uID, $address) {
	global $db;
	$ordID= _getCartID($uID);
	$sql = "update userorder set orderDate=now(),address=?,status=1 where ordID=?;";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "si", $address, $ordID); //bind parameters with variables
	return mysqli_stmt_execute($stmt);  //執行SQL
}

function shipout($ordID) {
	global $db;
	$sql = "update userorder set status=2 where ordID=?;";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	return mysqli_stmt_execute($stmt);  //執行SQL
}

function getCartDetail($uID) {
	global $db;
	$ordID= _getCartID($uID);
	$sql="select orderItem.serno, product.name, product.price, orderItem.quantity from orderItem, product where orderItem.prdID=product.prdID and orderItem.ordID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}


function getOrderDetail($ordID) {
	global $db;
	$sql="select orderItem.serno, product.name, product.price, orderItem.quantity from orderItem, product where orderItem.prdID=product.prdID and orderItem.ordID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}
?>










